import React, { useState } from 'react';
import DatePicker from './components/datepicker';

function App() {
  const [data, setData] = useState('');
  return (
    <div>
      <input type="text" value={data}></input>
      <DatePicker onChange={setData}/>
      
    </div>
  );
}

export default App;
