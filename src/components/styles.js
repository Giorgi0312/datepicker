import styled from 'styled-components'


export const Calendar = styled.div`
  background: #c4ffd9;
  display: block;
  width: 294px
`


export const WeekDayBox = styled.div`
  background: rgba(0,0,0,.2);
  width: 40px;
  font-family: sans-serif;
  font-size: 15px;
  text-align: center;
  line-height: 25px;
  border: 1px solid #fff;
  float: left
`

export const WeekDaysBar = styled.div`
  
`

export const CalendarTable = styled.span`
  
`

export const Day = styled.span`
  width: 40px;
  display: block;
  font-family: sans-serif;
  font-size: 15px;
  text-align: center;
  line-height: 25px;
  border: 1px solid #ccc;
  float: left
`