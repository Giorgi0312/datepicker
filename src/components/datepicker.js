import React, { useState } from 'react';
import moment from 'moment';
import 'moment/locale/pt-br'
import { WeekDayBox, WeekDaysBar, CalendarTable, Calendar, Day } from './styles';

// import { Container } from './styles';

export default function DatePicker(props) {
  
  moment.locale('pt-br');
  const now = moment()
  const [ year, setYear ] = useState(now.year())
  const [ month, setMonth ] = useState(now.month())

  const diasMes = []

  const getUltimoDiaMes = (year, month) => {
    let date = moment({month, year, day: 1});
    date = date.add(1, 'month')
    date = date.subtract(1, 'day')
    return Number(date.format('DD'))
  }

  const ultimoDiaMes = getUltimoDiaMes(year, month)
  
  for(let i = 1; i <= ultimoDiaMes; i++) {
    diasMes.push(i)
  }

  const getDiaSemanaPrimeiroDia = () => {
    let date = moment({month, year, day: 1});
    return date.weekday()
  }

  const updateMonth = (dif) => {
    if (month === 0 && dif === -1) {
      setYear(year -1)
      setMonth(11)
    } else if (month === 11 && dif === 1) {
      setYear(year + 1)
      setMonth(0)
    } else {
      setMonth(month+dif)
    }
  }

  const onClick = (date) => {
    props.onChange(date)
  }
  return (
    <Calendar>
      <button onClick={() => {
        setMonth(moment().month())
        setYear(moment().year())
      }}>hoje</button>
      <div>
        <button onClick={() => {
          setYear(year-1)
        }}>anterior</button>
        { year }
        <button onClick={() => {
          setYear(year+1)
        }}>proximo</button>
      </div>
      <div>
        <button onClick={() => {
          updateMonth(-1)
        }}>anterior</button>
        { moment.months()[month] }
        <button onClick={() => {
          updateMonth(1)
        }}>proximo</button>
      </div>    
      <WeekDaysBar>
        { moment.weekdaysShort().map(
          (el, i) => <WeekDayBox key={i}>{el}</WeekDayBox>
        )}
      </WeekDaysBar>
      <CalendarTable>
      {Array(getDiaSemanaPrimeiroDia()).fill(0).map((el, i) => <Day></Day>)}
      {diasMes.map(
          (el, i) => <Day key={i} onClick={() => {
            const data = moment({day: el, month, year})
            onClick(data.format('DD/MM/YYYY'))
          }}>{el}</Day>
        )}
      </CalendarTable>
    </Calendar>
  );
}
